package by.izasemkova.logsanalysistoll;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;

/**
 * Analyzes records from files from BlockingQueue. If they fits given filtering
 * options, puts this records to final BlockingQueue for further processing.
 *
 * @version 1.0 26/11/2016
 * @author Irina Zasemkova
 */
public class AnalyzeFilesTask implements Runnable {

    private final static Logger LOGGER = Logger.getLogger(AnalyzeFilesTask.class);

    private final BlockingQueue<File> queue;
    private final BlockingQueue<String> resultsLogQueue;
    private final String timePeriodBegin;
    private final String timePeriodEnd;
    private final String username;
    private final String messagePattern;

    public AnalyzeFilesTask(BlockingQueue<File> queue, String timePeriodBegin, String timePeriodEnd, String username, String messagePattern, BlockingQueue<String> resultsLogQueue) {
        this.queue = queue;
        this.timePeriodBegin = timePeriodBegin;
        this.timePeriodEnd = timePeriodEnd;
        this.username = username;
        this.messagePattern = messagePattern;
        this.resultsLogQueue = resultsLogQueue;
    }

    @Override
    public void run() {
        try {
            boolean done = false;
            while (!done) {
                File file = queue.take();
                if (file == GetFilesListTask.DUMMY) {
                    queue.put(file);
                    resultsLogQueue.put(LogAnalysis.STOP_THREAD_MARKER);
                    done = true;
                } else {
                    analyze(file);
                }
            }
        } catch (InterruptedException e) {
            LOGGER.error("Thread was interrupted.; " + e.getMessage());
        }
    }

    private void analyze(File file) throws InterruptedException {
        try (Scanner in = new Scanner(new BufferedInputStream(new FileInputStream(file)))) {
            while (in.hasNextLine()) {
                String line = in.nextLine();

                String[] lineElements = line.split(LogAnalysis.LINE_SPLIT_MARKER);

                boolean inPeriod = inPeriod(lineElements[0].trim(), timePeriodBegin, timePeriodEnd);
                boolean recuiredUsername = recuiredUsername(lineElements[1].trim(), username);
                boolean hasRequedMessage = hasRequedMessage(lineElements[2].trim(), messagePattern);

                if (inPeriod && recuiredUsername && hasRequedMessage) {
                    resultsLogQueue.put(line);
                }
            }
        } catch (FileNotFoundException e) {
            LOGGER.error("File " + file + " not found.; " + e.getMessage());
        }
    }

    private boolean recuiredUsername(String logUsername, String username) {

        if (username.isEmpty()) {
            return true;
        }

        boolean hasUsername = false;

        if (logUsername.equals(username)) {
            hasUsername = true;
        }

        return hasUsername;
    }

    private boolean inPeriod(String logDate, String timePeriodBegin, String timePeriodEnd) {

        if (timePeriodBegin.isEmpty() && timePeriodEnd.isEmpty()) {
            return true;
        } else if (timePeriodBegin.isEmpty()) {
            timePeriodBegin = AdditionalTask.getTimePeriodBeginValue();
        } else if (timePeriodEnd.isEmpty()) {
            timePeriodEnd = AdditionalTask.getTimePeriodEndValue();
        }

        boolean inPeriod = false;

        Date startDate = AdditionalTask.getDateFromString(timePeriodBegin);
        Date endDate = AdditionalTask.getDateFromString(timePeriodEnd);
        Date date = AdditionalTask.getDateFromString(logDate);

        if (date.after(startDate) && date.before(endDate)) {
            inPeriod = true;
        }
        return inPeriod;
    }

    private boolean hasRequedMessage(String logMessage, String messagePattern) {

        if (messagePattern.isEmpty()) {
            return true;
        }

        boolean hasRequedMessage = false;

        Pattern pattern = Pattern.compile(messagePattern);

        Matcher matcher = pattern.matcher(logMessage);
        if (matcher.find()) {
            hasRequedMessage = true;
        }
        return hasRequedMessage;
    }

}
