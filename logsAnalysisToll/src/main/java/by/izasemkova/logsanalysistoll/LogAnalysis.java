package by.izasemkova.logsanalysistoll;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.apache.log4j.Logger;

/**
 * @version 1.0 26/11/2016
 * @author Irina Zasemkova
 */
public class LogAnalysis {

    private final static Logger LOGGER = Logger.getLogger(LogAnalysis.class);

    public final static String STOP_THREAD_MARKER = "STOP";
    public final static String LINE_SPLIT_MARKER = "\\|\\|";
    public final static String INPUT_FILE = "input.txt";

    public final static String USERNAME_PARAMETER = "username";
    public final static String TIME_PERIOD_BEGIN_PARAMETER = "timePeriodBegin";
    public final static String TIME_PERIOD_END_PARAMETER = "timePeriodEnd";
    public final static String MESSAGE_PATTERN = "messagePattern";
    public final static String USERNAME_FLAG = "usernameFlag";
    public final static String TIME_UNIT = "timeUnit";
    public final static String SEARCH_THREADS = "searchThreads";
    public final static String OUTPUT_FILE = "outputFile";
    public final static String INPUT_DIRECTORY = "inputDirectory";

    /**
     * This method gets input parameters from file and calls threads which will
     * read files from directory, analyze them and write output information.
     *
     * @param args
     */
    public static void main(String[] args) {

        List<String> parametersList = new ArrayList<String>();

        

        try (Scanner scanner = new Scanner(new File(INPUT_FILE))) {
            while (scanner.hasNextLine()) {
                String parameterString = scanner.nextLine();
                parametersList.add(parameterString);
            }
        } catch (FileNotFoundException e) {
            LOGGER.error("File " + INPUT_FILE + " not found.; " + e.getMessage());
        }

        Map<String, String> parameters = new HashMap<String, String>();

        for (String line : parametersList) {
            String[] parts = line.split("=");
            String key = parts[0];
            String value = "";
            if (parts.length == 2) {
                value = parts[1];
            }
            parameters.put(key, value);
        }

        String username = parameters.get(USERNAME_PARAMETER);
        String timePeriodBegin = parameters.get(TIME_PERIOD_BEGIN_PARAMETER);
        String timePeriodEnd = parameters.get(TIME_PERIOD_END_PARAMETER);
        String messagePattern = parameters.get(MESSAGE_PATTERN);

        boolean usernameFlag = false;
        if ("yes".equals(parameters.get(USERNAME_FLAG))) {
            usernameFlag = true;
        }
        String timeUnit = parameters.get(TIME_UNIT);

        int searchThreads = 1;
        try {
            searchThreads = Integer.parseInt(parameters.get(SEARCH_THREADS));
        } catch (NumberFormatException e) {
            LOGGER.error("Parameter  'searchThreads' not a number.; " + e.getMessage());
        }
        String outputFile = parameters.get(OUTPUT_FILE);
        String inputDirectory = parameters.get(INPUT_DIRECTORY);

        int queueSize = searchThreads;

        BlockingQueue<File> logFilesQueue = new ArrayBlockingQueue<File>(queueSize);
        BlockingQueue<String> resultsLogQueue = new ArrayBlockingQueue<String>(queueSize);

        GetFilesListTask reader = new GetFilesListTask(logFilesQueue, new File(inputDirectory));
        File filtredLogsFile = new File(outputFile);
        if (!filtredLogsFile.exists()) {
            try {
                filtredLogsFile.getParentFile().mkdirs();
                filtredLogsFile.createNewFile();
            } catch (IOException e) {
                LOGGER.error("Probles with file creation.; " + e.getMessage());
            }
        }
        FileWriterTask writer = new FileWriterTask(resultsLogQueue, filtredLogsFile, searchThreads, usernameFlag, timeUnit);

        new Thread(reader).start();
        for (int i = 1; i <= searchThreads; i++) {
            new Thread(new AnalyzeFilesTask(logFilesQueue, timePeriodBegin, timePeriodEnd, username, messagePattern, resultsLogQueue)).start();
        }
        new Thread(writer).start();
    }

}
