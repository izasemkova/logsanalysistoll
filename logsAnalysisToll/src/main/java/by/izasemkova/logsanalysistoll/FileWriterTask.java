package by.izasemkova.logsanalysistoll;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import org.apache.log4j.Logger;

/**
 * Gets records from final BlockingQueue and writes them to the output file.
 * Then creates statistic using input grouping parameters and prints it to the
 * console.
 *
 * @version 1.0 26/11/2016
 * @author Irina Zasemkova
 */
public class FileWriterTask implements Runnable {

    private final static Logger LOGGER = Logger.getLogger(FileWriterTask.class);

    public final static String LINE_SEPARATOR = System.getProperty("line.separator");
    public final static String NAME_RECORDS_OUTPUT_FORMAT = "%s - %d records %n";
    public final static String NAME_TIMEUNIT_RECORDS_OUTPUT_FORMAT = "%s : %s - %d records %n";
    public final static String TIMEUNIT_RECORDS_OUTPUT_FORMAT = "%s - %d records %n";

    public final static String YEAR_FORMAT = "yyyy";
    public final static String MONTH_FORMAT = "MMM, yyyy";
    public final static String DAY_FORMAT = "MMM dd, yyyy";
    public final static String HOUR_FORMAT = "MMM dd, yyyy HH";

    private final BlockingQueue<String> queue;
    private final File outputFile;
    private final int searchThreads;
    private final boolean usernameFlag;
    private final String timeUnit;

    private final List<String> recordsList = new ArrayList<String>();

    public FileWriterTask(BlockingQueue<String> queue, File outputFile, int searchThreads, boolean usernameFlag, String timeUnit) {
        this.queue = queue;
        this.outputFile = outputFile;
        this.searchThreads = searchThreads;
        this.usernameFlag = usernameFlag;
        this.timeUnit = timeUnit;
    }

    @Override
    public void run() {
        try {
            writeOutputFile(outputFile, queue, searchThreads);
            if (usernameFlag) {
                Set<String> nameSet = getUsernamesSet();
                writeStatistic(nameSet, timeUnit);
            } else {
                writeStatistic(timeUnit);
            }

        } catch (InterruptedException e) {
            LOGGER.error("Thread was interrupted.; " + e.getMessage());
        }

    }

    private void writeOutputFile(File outputFile, BlockingQueue<String> queue, int searchThreads) throws InterruptedException {

        try (FileWriter writeFile = new FileWriter(outputFile)) {
            boolean done = false;
            int finishedThreadsCounter = 0;
            while (!done) {
                String line = queue.take();
                if (LogAnalysis.STOP_THREAD_MARKER.equals(line)) {
                    finishedThreadsCounter++;
                    if (finishedThreadsCounter == searchThreads) {
                        done = true;
                    }
                } else {
                    writeFile.append(line);
                    writeFile.append(LINE_SEPARATOR);
                    recordsList.add(line);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Probles with file creation.; " + e.getMessage());
        }
    }

    private Set<String> getUsernamesSet() throws InterruptedException {
        Set<String> nameSet = new HashSet<String>();
        for (String record : recordsList) {
            String name = getName(record);
            nameSet.add(name);
        }

        return nameSet;
    }

    private String getName(String line) {
        String[] lineElements = line.split(LogAnalysis.LINE_SPLIT_MARKER);

        return lineElements[1].trim();
    }

    private void writeStatistic(Set<String> nameSet, String timeUnit) {
        if (timeUnit.isEmpty()) {
            for (String recordName : nameSet) {
                int recordsCounter = 0;
                for (String record : recordsList) {
                    String name = getName(record);
                    if (name.equals(recordName)) {
                        recordsCounter++;
                    }
                }
                if (recordsCounter != 0) {
                    System.out.printf(NAME_RECORDS_OUTPUT_FORMAT, recordName, recordsCounter);
                }
            }
        } else {
            DateFormat dateFormat = setDateFormat(timeUnit);
            for (String recordName : nameSet) {
                List<Date> timeList = new ArrayList<Date>();
                for (String record : recordsList) {
                    String name = getName(record);
                    if (name.equals(recordName)) {
                        timeList.add(getDate(record));
                    }
                }

                Date[] timePeriod = AdditionalTask.setTimePeriod(timeList, timeUnit);

                for (Date date = timePeriod[0]; date.before(timePeriod[1]); date = changeDate(date, timeUnit)) {
                    int recordsCounter = 0;
                    for (Date recordDate : timeList) {
                        if (((recordDate.after(date) || (recordDate.equals(date))) && recordDate.before(changeDate(date, timeUnit)))) {
                            recordsCounter++;
                        }
                    }
                    if (recordsCounter != 0) {
                        System.out.printf(NAME_TIMEUNIT_RECORDS_OUTPUT_FORMAT, recordName, dateFormat.format(date), recordsCounter);
                    }
                }
            }
        }
    }

    private void writeStatistic(String timeUnit) {
        DateFormat dateFormat = setDateFormat(timeUnit);
        List<Date> timeList = new ArrayList<Date>();
        for (String record : recordsList) {
            timeList.add(getDate(record));
        }

        Date[] timePeriod = AdditionalTask.setTimePeriod(timeList, timeUnit);

        for (Date date = timePeriod[0]; date.before(timePeriod[1]); date = changeDate(date, timeUnit)) {
            int recordsCounter = 0;
            for (Date recordDate : timeList) {
                if (((recordDate.after(date) || (recordDate.equals(date))) && recordDate.before(changeDate(date, timeUnit)))) {
                    recordsCounter++;
                }
            }
            if (recordsCounter != 0) {
                System.out.printf(TIMEUNIT_RECORDS_OUTPUT_FORMAT, dateFormat.format(date), recordsCounter);
            }
        }
    }

    private Date changeDate(Date date, String timeUnit) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        switch (timeUnit.toLowerCase()) {
            case "year":
                c.add(Calendar.YEAR, 1);
                break;
            case "month":
                c.add(Calendar.MONTH, 1);
                break;
            case "day":
                c.add(Calendar.DAY_OF_MONTH, 1);
                break;
            case "hour":
                c.add(Calendar.HOUR_OF_DAY, 1);
                break;
            default:
                c.add(Calendar.MONTH, 1);
                break;
        }
        return c.getTime();
    }

    private Date getDate(String line) {
        String[] lineElements = line.split(LogAnalysis.LINE_SPLIT_MARKER);
        String logDate = lineElements[0].trim();

        return AdditionalTask.getDateFromString(logDate);
    }

    private DateFormat setDateFormat(String timeUnit) {
        DateFormat dateFormat = null;
        switch (timeUnit) {
            case "year":
                dateFormat = new SimpleDateFormat(YEAR_FORMAT);
                break;
            case "month":
                dateFormat = new SimpleDateFormat(MONTH_FORMAT);
                break;
            case "day":
                dateFormat = new SimpleDateFormat(DAY_FORMAT);
                break;
            case "hour":
                dateFormat = new SimpleDateFormat(HOUR_FORMAT);
                break;
            default:
                dateFormat = new SimpleDateFormat(MONTH_FORMAT);
                break;
        }

        return dateFormat;
    }
}
