package by.izasemkova.logsanalysistoll;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Contains methods which helps work with Date.
 *
 * @version 1.0 26/11/2016
 * @author Irina Zasemkova
 */
public class AdditionalTask {

    private final static Logger LOGGER = Logger.getLogger(AdditionalTask.class);

    public final static String DATE_FORMAT = "MMM dd, yyyy HH:mm:ss";
    private final static String TIME_PERIOD_BEGIN_VALUE = "Jan 1, 2000 00:00:00";

    /**
     * @return string with initial time
     */
    public static String getTimePeriodBeginValue() {
        return TIME_PERIOD_BEGIN_VALUE;
    }

    /**
     * Represents current date in required format.
     *
     * @return string with end time
     */
    public static String getTimePeriodEndValue() {
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

        return dateFormat.format(new Date());
    }

    /**
     * Parses date/time string using required format.
     *
     * @param dateInString string with date
     * @return
     */
    public static Date getDateFromString(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        Date date = null;
        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            LOGGER.error("Problems with parsing Date from string.; " + e.getMessage());
        }

        return date;
    }

    /**
     * Sets initial and final dates for dates from list.
     *
     * @param timeList list with dates
     * @param timeUnit time interval (e.g. hour, dey, month, year)
     * @return array with initial and final dates
     */
    public static Date[] setTimePeriod(List<Date> timeList, String timeUnit) {
        Date startDate = timeList.get(0);
        Date endDate = timeList.get(0);
        for (Date date : timeList) {
            if (date.before(startDate)) {
                startDate = date;
            }
            if (date.after(endDate)) {
                endDate = date;
            }
        }
        Date[] timePeriod = new Date[2];
        timePeriod[0] = setPeriod(startDate, timeUnit, 0);
        timePeriod[1] = setPeriod(endDate, timeUnit, 1);

        return timePeriod;
    }

    private static Date setPeriod(Date startDate, String timeUnit, int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        switch (timeUnit.toLowerCase()) {
            case "year":
                calendar.set(Calendar.YEAR, year);
                calendar.add(Calendar.YEAR, amount);
                calendar.set(Calendar.MONTH, 0);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                break;
            case "month":
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.add(Calendar.MONTH, amount);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                break;
            case "day":
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);
                calendar.add(Calendar.DAY_OF_MONTH, amount);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                break;
            case "hour":
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.add(Calendar.HOUR_OF_DAY, amount);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                break;
            default:
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.add(Calendar.MONTH, amount);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
        }

        return calendar.getTime();
    }

}
