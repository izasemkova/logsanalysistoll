package by.izasemkova.logsanalysistoll;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import org.apache.log4j.Logger;

/**
 * Gets files with required extention from given directory and put them to the BlockingQueue.
 * 
 * @version 1.0 26/11/2016
 * @author Irina Zasemkova
 */
public class GetFilesListTask implements Runnable {

    private final static Logger LOGGER = Logger.getLogger(GetFilesListTask.class);

    public static File DUMMY = new File("");
    public static String FILE_TYPE = ".log";

    private final BlockingQueue<File> queue;
    private final File directory;

    public GetFilesListTask(BlockingQueue<File> queue, File directory) {
        this.queue = queue;
        this.directory = directory;
    }

    @Override
    public void run() {
        try {
            getFilesList(directory);
            queue.put(DUMMY);
        } catch (InterruptedException e) {
            LOGGER.error("Thread was interrupted.; " + e.getMessage());
        }
    }

    private void getFilesList(File directory) throws InterruptedException {
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                getFilesList(file);
            } else if (file.getName().toLowerCase().endsWith(FILE_TYPE)) {
                queue.put(file);
            }
        }
    }

}
